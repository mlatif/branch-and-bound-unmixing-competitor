% USGS_GENERATOR
% Problem instance generator for K, Q, SNR coefficients given as parameters
% and a spectral dictionary H (size(H) = N x Q)
% Formulation : 
%       min ||y-Hx||_2^2 st. 1_Q*x = 1; ||x||_0 <= K; x >= 0 
%       with y = Hx + e*randn(1_N) (noisy observations)
%            e = [(||w||_2^2 / N)*10.^(-SNR/10)]^1/2 and w = Hx
% INPUT :
%     - G struct with
%           G.dict [NxQ] : the spectral dictionary (eg. load('usgs_library.mat').spec)
%           G.set_name   : the folder name where the generated instances are stored (eg. "SET_1")
%           G.seed       : trivial (eg. 'default');
%           G.P          : the number of instances for a given (Q,K,SNR) configuration (eg. P = 1)
%                           <=> the permutation number of the G.dict
%           G.tau        : The minimum significance threshold on the abundances in the generated models (eg. tau = .1)
%                           <=> x_i >= tau \forall i \in |[1,Q]|      
%           G.K_rng      : The set of desired sparsity coefficients (eg. K_rng = [2:6])
%           G.Q_rng      : The set of desired number of endmembers (eg. Q_rng = [20,50,100,200,300,400,498])
%           G.SNR_rng    : The set of desired additional noise in the model (eg. SNR_rng = [40:5:60,inf])
% OUTPUT :
%     - I struct with 
%           I.SNR_table : a talbe where each column contains the name of the instances for a given SNR level
%           I.Q_table : same idea for the number of endmembers
%           I.K_table : same idea for the sparsity coefficient;
% EXAMPLE : 
%   G.dict = load('usgs_library.mat').spec;
%   G.set_name = "SET_1";
%   G.seed = 'default';
%   G.P = 1;
%   G.tau = 0.1;
%   G.K_rng = [2:6];
%   G.Q_rng = [20,50,100,200,300,400,498];
%   G.SNR_rng = [40:5:60,inf];
%   >> USGS_GENERATOR(G)
%   will generate : G.P * card(G.SNR_rng) * card(G.K_rng) * card(G.Q_rng) = 210 instances
% REMARK :
%     - In order to be compatible with the sparse MIP optimization framework, the following empty files are generated  
%           alpha_bruit.dat, alpha_noise_stat.dat, lambda_stat.dat, lambda.dat, sigma.dat 
%     - Convention : SNR = \infty is denoted by SNR100
%     - folder notations :
%           - the instances are stored in the corresponding folder : ASC_ANC_INST_"G.set_name"/
%           - an instance folder is named : SA_SNR[s]_K[k]_instance[i] where s is the SNR level, k is the sparsity level, i is the number of generated instance    

function [I] = USGS_GENERATOR(G)
    rng(G.seed);
    path=strcat("ASC_ANC_INST_",G.set_name,"/");
    prefix_inst = "SA_SNR";
    if(~exist(path)),  mkdir(path); end
    % ---
    S = G.dict;
    [N,Q] = size(S);
    save(strcat(path,'Hmatrix.dat'),'S','-ASCII');
    
    n_conf = G.P * length(G.SNR_rng) * length(G.K_rng) * length(G.Q_rng);
    I.SNR_table = strings(n_conf,length(G.SNR_rng));
    I.Q_table = strings(n_conf,length(G.Q_rng));
    I.K_table = strings(n_conf,length(G.K_rng));

    % ---
    for (m = 1:length(G.SNR_rng))
        snr = G.SNR_rng(m);
        for (n = 1:length(G.K_rng))
            k = G.K_rng(n);
            for (o = 1:length(G.Q_rng))
                q = G.Q_rng(o);
                fprintf("SNR = %d \t K = %d \t Q = %d \n", snr, k,q)
                for (i = 1:G.P)
                    perm = randperm(Q);
                    ind_colonnes = perm(1:q);
                    H = S(:,ind_colonnes);
                    perm_Q = randperm(q);
                    ind_NZ_x = perm_Q(1:k);
                    % ---
                    x_truth = zeros(q,1);
                    all_sup_tau = false;
                    while (~all_sup_tau), u = rand(k,1);  x_NZ = u/sum(u); all_sup_tau = all(x_NZ>=G.tau);  end
                    x_truth(ind_NZ_x) = x_NZ;
                    % ---
                    y_wo_noise = H*x_truth;
                    sigma_vec = sqrt(norm(y_wo_noise)^2/length(y_wo_noise)*10.^(-snr/10));
                    y = H*x_truth + sigma_vec*randn(N,1);
                    % ---
                    pSNR = snr;
                    if (isinf(snr)),  pSNR = 100; end
                    % ---
                    part_name = strcat(path,prefix_inst,num2str(pSNR),"_K",num2str(k),"_instance");
                    d = dir(strcat(part_name,'*'));
                    n_inst = size(d,1);
                    file_name = strcat(part_name,num2str(n_inst+1),'/');
                    mkdir(file_name);
                    % ---
                    S1 = find(x_truth);
                    info_file = strcat(file_name,"info.txt");
                    fileID = fopen(info_file,"w+");
                        fprintf(fileID,"%s \n",erase(file_name,[path,"/"]));
                        fprintf(fileID,"\tQ = %d\n",q);
                        fprintf(fileID,"\tS1 : "); fprintf(fileID,repmat('%d, ', 1, length(S1)), S1);fprintf(fileID,"\n");
                        for x = 1:length(S1), fprintf(fileID,"\t\tx(%d) = %.5f \n ",S1(x),x_truth(S1(x))); end
                        fprintf(fileID,"\terr_l2 = %.7e\n",norm(y-H*x_truth)^2);
                    fclose(fileID);
                    % ---
                    dflt_value = 0;
                    % save(strcat(file_name,'/alpha_bruit.dat'),'dflt_value','-ASCII');
                    % save(strcat(file_name,'/lambda.dat'),'dflt_value','-ASCII');
                    % save(strcat(file_name,'/alpha_noise_stat.dat'),'dflt_value','-ASCII');
                    % save(strcat(file_name,'/lambda_stat.dat'),'dflt_value','-ASCII');
                    save(strcat(file_name,'/y.dat'),'y','-ASCII');
                    save(strcat(file_name,'/Hmatrix.dat'),'H','-ASCII');
                    save(strcat(file_name,'/x_truth.dat'),'x_truth','-ASCII');
                    
            
                    key = find(I.SNR_table(:,m)==""); I.SNR_table(key(1),m)=erase(file_name,[path,"/"]);
                    key = find(I.K_table(:,n)==""); I.K_table(key(1),n)=erase(file_name,[path,"/"]);
                    key = find(I.Q_table(:,o)==""); I.Q_table(key(1),o)=erase(file_name,[path,"/"]);
                    
                    test = "";
                end % for i = 1:G.P

            end % for (q = G.Q_rng)
        end % for (k = G.K_rng)
    end % for (snr = G.SNR_rng)
    I.K_table = array2table(I.K_table);
    I.Q_table = array2table(I.Q_table);
    I.SNR_table = array2table(I.SNR_table);
    save(strcat(path,"instances_tables"),'I');    
end
