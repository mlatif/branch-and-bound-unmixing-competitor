% GET_MODEL
% Get the FCLS matrix formulation given H and y
% REMARK :
%     - Extension of the initial formulation to manage over/under constrained problem & prevent matlab's 5002 error (non convex problem)
%       [N,Q]=size(H) :
%           - case  N > Q : min 0.5||y-H*x||_2^2 st. sum(x) = 1, x >= 0;
%           - case  N < Q : min 0.5||y-z  ||_2^2 st. sum(x) = 1, x >= 0, z = Hx.
% INPUT :
%     - H is the dictionary
%     - y the spectrum
%     - asc_form : the formulation of the sum-to-one constraint (eq or in)
% OUTPUT :
%     - HtH,ytH : the values of the cost function st. 0.5*x'*HtH*x-ytH*x
%     - x0 : column vector of initial point of x
%     - A_ineq, b_ineq : matrix/vector for linear inequality constraints
%     - A_eq, b_eq : matrix/vector for linear equality constraints
%     - lb/ub : column vector of lower/upper bounds
%     - x_status : indicator vectors of the abundances variables 
%           - 1 <=> abundance variables x 
%           - 0 <=> cost function reformulation variables 
function [HtH,ytH,x0,A_ineq,b_ineq,A_eq,b_eq,ub,lb,x_status] = GET_FCLS_MODEL(H,y,asc_form)
    [N,Q] = size(H);
    if (N > Q)
        HtH = 2*(H'*H); ytH = 2*y'*H;
        if(strcmp(asc_form,"in"))
            A_ineq = ones(1,Q); b_ineq = 1; A_eq = []; b_eq = [];
        else
            A_ineq = []; b_ineq = [];   A_eq = ones(1,Q); b_eq = 1;
        end
        lb = zeros(Q,1); ub = ones(Q,1);
        x0 = zeros(Q,1);
        x_status = true(Q,1);
    else
        C = sparse([zeros(N,Q),speye(N)]);
        HtH = C'*C; ytH = y'*C;
        lb = [zeros(Q,1); -inf(N,1)]; ub = [ones(Q,1); +inf(N,1)];
        x0 = [zeros(Q,1); H*zeros(Q,1)];
        A_eq = [H, -eye(N)];
        b_eq = zeros(N,1);
        if(strcmp(asc_form,"in"))
            A_ineq = [ones(1,Q),zeros(1,N)];    b_ineq = 1;
        else
            A_eq = [A_eq; ones(1,Q), zeros(1,N)];   b_eq = [b_eq; 1];
        end
        x_status = [true(Q,1); false(N,1)];
    end
end