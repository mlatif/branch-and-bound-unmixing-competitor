% SDA
% Code for sparse demixing based on iteratively removing the smallest
% SOURCE :
%     Sparse Demixing of Hyperspectral Images (John B. Greer)
%     IEEE TRANSACTIONS ON IMAGE PROCESSING, VOL. 21, NO. 1, JANUARY 2012
%     Sparse Demixing algorithm in Fig. 4
% REMARK :
%     - Cplex solver is used to solve the FCLS problem
% INPUT :
%     - H is the dictionary
%     - y the spectrum
%     - K the maximum number of non-zero components allowed.
%     - asc_form : the formulation of the sum-to-one constraint (eq or in)
%     - tol_zero : numerical threshold to infer a zero value. 
% OUTPUT :
%     - x is the K-sparse solution
%     - it the number of iterations
% CREDITS :
%     Designed by J.B.Greer, implemented by S.Bourguignon, updated by M.Latif;
function [x,it] = SDA(H,y,K,asc_form,tol_zero)
    x = []; it = 0; is_Ksp = false;
    options = cplexoptimset;    options.Display = 'off';
    % ---
    [HtH,ytH,x0,A_ineq,b_ineq,A_eq,b_eq,ub,lb,x_status] = GET_FCLS_MODEL(H,y,asc_form);
    [x_prime,~,~,~] = cplexqp(HtH,-ytH,A_ineq,b_ineq,A_eq,b_eq,lb,ub,x0,options);
    x_prime = x_prime(x_status);
    ub(x_prime<tol_zero) = 0;
    % ---
    while (~is_Ksp)
        ind1 = find(x_prime>tol_zero);
        ub(x_prime<=tol_zero) = 0;
        if (sum(ub) > K)
            [~,indmin] = min(x_prime(ind1));
            ub(ind1(indmin)) = 0;
            if (ind1(indmin) > size(H,2)), break;   end
            it=it+1;
        end
        [x_prime,~,~,~] = cplexqp(HtH,-ytH,A_ineq,b_ineq,A_eq,b_eq,lb,ub,[],options);
        x_prime = x_prime(x_status);
        is_Ksp = sum(x_prime>0)<=K;
        if (is_Ksp)
            [x,~,~,~] = cplexqp(HtH,-ytH,A_ineq,b_ineq,A_eq,b_eq,lb,ub,[],options);
            x = x(x_status);
        end
    end
end


