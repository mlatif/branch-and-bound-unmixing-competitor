% K_FCLS
% Two-phase algorithm to perform a first estimation of the FCLS solution and then to select the K maximum amplitude components.
% SOURCE :
%     
% REMARK :
%     - Cplex solver is used to solve the FCLS problem
% INPUT :
%     - H is the dictionary
%     - y the spectrum
%     - K the maximum number of non-zero components allowed.
%     - asc_form : the formulation of the sum-to-one constraint (eq or in)
%     - tol_zero : numerical threshold to infer a zero value. 
% OUTPUT :
%     - X is the K-sparse solution
%     - X_F is the FCLS solution before the selection of the K best atoms 
% CREDITS :
%     Implemented by S.Bourguignon, updated by M.Latif;


function [X,X_F] = K_FCLS(H,y,K,asc_form,tol_zero)    
    options = cplexoptimset;    options.Display = 'off';
    % ---
    [HtH,ytH,x0,A_ineq,b_ineq,A_eq,b_eq,ub,lb,x_status] = GET_FCLS_MODEL(H,y,asc_form);
    % ---
    [x,~,~,~] = cplexqp(HtH,-ytH,A_ineq,b_ineq,A_eq,b_eq,lb,ub,x0,options);
    X_F = x(x_status);
    X_F(X_F<tol_zero) = 0;
    % ---
    if (sum(X_F~=0) <= K)
        X = X_F;
    else
        [~,I] = sort(X_F,'descend');
        supp = sort(I(1:K));    
        ub(setdiff(1:size(H,2),supp)) = 0;
        % ---
        [x,~,~,~] = cplexqp(HtH,-ytH,A_ineq,b_ineq,A_eq,b_eq,lb,ub,x0,options);
        X = x(x_status);
        X(X<tol_zero) = 0;
    end
end



