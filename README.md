# Branch-and-bound - Unmixing - Competitor

## Instances simulées :
[Lien de téléchargement](https://uncloud.univ-nantes.fr/index.php/s/jbRgonfZ7bHgMmg)
Pour les données que nous utilisons pour les résultats, nous avons
- 2 jeux de données :
	- ASC_ANC_INST_SET_1, seed = 'default'
    - ASC_ANC_INST_SET_2, seed = '280192'
- Chaque SET est généré avec des valeurs
	- de Q \in {20,50,100,200,300,400,498}
	- de K \in |[2,6]|
	- de SNR \in {40,45,50,55,60,infty}
    - un seuil de significativité sur les amplitudes : tau = 0.1;
    - 20 instances par configuration (Q,K,SNR)
    - Soit 8400 instances sur lesquelles on peut réaliser des tests
    - Convention infty <=> 100
